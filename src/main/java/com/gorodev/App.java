package com.gorodev;

import org.apache.commons.lang3.StringUtils;

import java.util.Scanner;

public class App {

    public static void main(String[] args) {
        // создали бота
        var bot = new Bot();

        // попросили бота выдать сообщение при запуске
        System.out.println(bot.onStart());

        var reader = new Scanner(System.in);

        var message = reader.nextLine();
        while (StringUtils.isNotEmpty(message)) {
            // дали боту сообщение на обработку и сохранили результат в botResponse
            var botResponse = bot.onMessage(message);

            // вывели ответ бота на экран
            System.out.println(botResponse);

            message = reader.nextLine();
        }
    }
}