package com.gorodev;

import java.util.HashSet;
import java.util.Set;

class Vocabulary {

    Set<String> words = new HashSet<>();

    void add(String word) {
        words.add(word);
    }

    Set<String> getAll() {
        return words;
    }
}

