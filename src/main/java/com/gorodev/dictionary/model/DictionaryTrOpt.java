package com.gorodev.dictionary.model;

import lombok.Data;

import java.util.List;

@Data
public class DictionaryTrOpt {

    private Long id;
    private String tx;
    private String ts;
    private String tr;
    private String synonyms;
    private String partOfSpeech;
    private List<DictionaryTrEx> examples;

}