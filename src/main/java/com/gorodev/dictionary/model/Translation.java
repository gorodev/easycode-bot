package com.gorodev.dictionary.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
public class Translation {

    private List<DictionaryTrOpt> options = new ArrayList<>();

    @Override
    public String toString() {

        long orderNumber = 1;
        for (DictionaryTrOpt dictionaryTrOpt : options) {
            dictionaryTrOpt.setId(orderNumber++);
        }

        return "------------------------------\n" +
                options.stream()
                        .map(option -> {
                            String shortInfo = option.getTx() + " [" + option.getTs() + "] " + option.getTr();
                            String examples = option.getExamples().stream()
                                    .map(ex -> "> " + ex.getText() + " - " + ex.getTr()).
                                            collect(Collectors.joining("\n"));
                            String partOfSpeech = option.getPartOfSpeech();
                            String synonyms = option.getSynonyms();
                            return option.getId() + ". " + shortInfo
                                    + " (" + partOfSpeech + ")\n"
                                    + (StringUtils.isEmpty(examples) ? "" : "\n" + examples)
                                    + ((StringUtils.isNotEmpty(examples) && StringUtils.isNotEmpty(synonyms)) ? "\n\n" : "")
                                    + (StringUtils.isEmpty(synonyms) ? "" : "synonyms: " + synonyms);
                        })
                        .collect(Collectors.joining("\n\n"))
                + "\n------------------------------";
    }
}