package com.gorodev.dictionary.service;

import com.gorodev.dictionary.model.Language;
import com.gorodev.dictionary.providers.DictionaryProviderService;
import com.gorodev.dictionary.providers.yandex.service.YandexDictionaryProviderService;
import com.gorodev.dictionary.model.Translation;

import java.util.HashMap;
import java.util.Map;

public class Dictionary {

    private Dictionary() {
    }

    private static final Map<String, Translation> CACHE = new HashMap<>();

    private static final DictionaryProviderService DICTIONARY_PROVIDER_SERVICE =
            new YandexDictionaryProviderService();

    public static Translation translate(String word) {
        if (CACHE.containsKey(word)) return CACHE.get(word);

        Translation translation = DICTIONARY_PROVIDER_SERVICE.translate(Language.EN, Language.RU, word);
        CACHE.put(word, translation);
        return translation;
    }
}