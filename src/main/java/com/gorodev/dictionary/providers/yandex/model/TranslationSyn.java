package com.gorodev.dictionary.providers.yandex.model;

import lombok.Data;

@Data
public class TranslationSyn {

    private String text;
    private String pos;
    private String gen;

}