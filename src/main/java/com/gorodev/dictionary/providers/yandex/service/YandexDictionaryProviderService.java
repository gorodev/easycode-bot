package com.gorodev.dictionary.providers.yandex.service;

import com.gorodev.dictionary.model.DictionaryTrEx;
import com.gorodev.dictionary.model.DictionaryTrOpt;
import com.gorodev.dictionary.model.Language;
import com.gorodev.dictionary.model.Translation;
import com.gorodev.dictionary.providers.DictionaryProviderService;
import com.gorodev.dictionary.providers.yandex.model.TranslationAllDefinitions;
import com.gorodev.dictionary.providers.yandex.model.TranslationDefinition;
import com.gorodev.dictionary.providers.yandex.model.TranslationSyn;
import com.gorodev.dictionary.utils.HttpUtils;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;

public class YandexDictionaryProviderService implements DictionaryProviderService {

    private final static String DICTIONARY_API_ENDPOINT =
            "https://dictionary.yandex.net/api/v1/dicservice.json/";
    private static String DICTIONARY_KEY;

    static {
        try (InputStream input = YandexDictionaryProviderService.class.getClassLoader()
                .getResourceAsStream("app.properties")) {
            Properties prop = new Properties();
            prop.load(input);
            DICTIONARY_KEY = prop.getProperty("yandex-key");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Translation translate(Language from,
                                 Language to,
                                 String word) {

        List<TranslationDefinition> definitions = HttpUtils
                .getObject(getDictionaryUrl(from, to, word), TranslationAllDefinitions.class)
                .getDef();

        return definitions.isEmpty()
                ? null
                : toDictionaryItem(definitions);
    }

    //////////////////////////////////
    //           private            //
    //////////////////////////////////

    private Translation toDictionaryItem(final List<TranslationDefinition> definitions) {
        return new Translation(definitions.stream()
                .map(yaDefinition -> yaDefinition.getTr().stream()
                        .map(yaTrOpt -> {
                            DictionaryTrOpt trOpt = new DictionaryTrOpt();
                            trOpt.setTx(yaDefinition.getText());
                            trOpt.setTs(yaDefinition.getTs());
                            trOpt.setTr(yaTrOpt.getText());
                            trOpt.setPartOfSpeech(yaDefinition.getPos());
                            trOpt.setSynonyms(yaTrOpt.getSyn().stream()
                                    .map(TranslationSyn::getText)
                                    .collect(Collectors.joining(", ")));
                            trOpt.setExamples(yaTrOpt.getEx().stream()
                                    .map(translationExample ->
                                            new DictionaryTrEx(
                                                    translationExample.getTr().get(0).getText(),
                                                    translationExample.getText()))
                                    .collect(Collectors.toList()));
                            return trOpt;
                        }).collect(Collectors.toList())
                ).collect(Collectors.toList()).stream()
                .flatMap(List::stream)
                .collect(Collectors.toList()));
    }

    private String getDictionaryUrl(Language from, Language to, String word) {
        String langParam = (from.toString() + "-" + to.toString()).toLowerCase();
        return DICTIONARY_API_ENDPOINT
                + "lookup?lang=" + langParam
                + "&key=" + DICTIONARY_KEY
                + "&text=" + word
                + "&flags=4";
    }

}