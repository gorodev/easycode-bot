package com.gorodev.dictionary.providers.yandex.model;


import lombok.Data;

import java.util.LinkedList;
import java.util.List;


@Data
public class TranslationExample {

    private String text;
    private List<TranslationOption> tr = new LinkedList<>();

}
