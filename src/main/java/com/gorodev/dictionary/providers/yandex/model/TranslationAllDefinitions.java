package com.gorodev.dictionary.providers.yandex.model;

import lombok.Data;

import java.util.LinkedList;
import java.util.List;


@Data
public class TranslationAllDefinitions {

    private Object head;
    private List<TranslationDefinition> def = new LinkedList<>();

}