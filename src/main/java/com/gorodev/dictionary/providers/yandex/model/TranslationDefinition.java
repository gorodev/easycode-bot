package com.gorodev.dictionary.providers.yandex.model;

import lombok.Data;

import java.util.LinkedList;
import java.util.List;


@Data
public class TranslationDefinition {

    private String text;
    private String pos;
    private String ts;
    private List<TranslationOption> tr = new LinkedList<>();

}