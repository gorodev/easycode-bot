package com.gorodev.dictionary.providers.yandex.model;

import lombok.Data;

import java.util.LinkedList;
import java.util.List;

@Data
public class TranslationOption {

    private String text;
    private String pos;
    private List<TranslationSyn> syn = new LinkedList<>();
    private List<Mean> mean = new LinkedList<>();
    private List<TranslationExample> ex = new LinkedList<>();

}