package com.gorodev.dictionary.providers;

import com.gorodev.dictionary.model.Translation;
import com.gorodev.dictionary.model.Language;

public interface DictionaryProviderService {

    Translation translate(Language from, Language to, String word);

}
