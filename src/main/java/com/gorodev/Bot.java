package com.gorodev;

import com.gorodev.dictionary.service.Dictionary;

class Bot {
    /*
     * Similarly, with the moving of the logic from the App to the Bot,
     * we will unload the Bot a little bit more and
     * will move some logic from Bot to Vocabulary.
     *
     * Thus, logically we will have 3 main elements of our application:
     * - work with keyboard (App class)
     * - bot (Bot class)
     * - Dictionary (Vocabulary class)
     */
    private final Vocabulary vocabulary = new Vocabulary();


    String onStart() {
        return "Write the word in English, for example: \"cat \"";
    }

    String onMessage(String message) {
        final String outMessage;

        if ("/list".equals(message)) {
            // combine all words into one value
            outMessage = String.join("\n", vocabulary.getAll());
        } else {
            var translation = Dictionary.translate(message);
            if (translation == null) {
                outMessage = "I do not know such a word. Try another one.";
            } else {
                vocabulary.add(message);
                outMessage = translation + "\nPlease write to add more words.";
            }
        }
        return outMessage;
    }
}