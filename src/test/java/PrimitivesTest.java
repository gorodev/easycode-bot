import org.junit.Test;

import static org.assertj.core.api.Java6Assertions.assertThat;

public class PrimitivesTest {

    @Test
    public void should_overflow() {
        byte a = Byte.MAX_VALUE; // 128

        byte b = (byte) (a + 100); // не обращайте внимание на (byte) - это пока лишние детали.

        assertThat(b).isEqualTo((byte) -29);
    }

    @Test
    public void should_lose_precision() {
        double sum = 0;
        for (int i = 0; i < 33; i++) {
            sum += (double) 1 / 33;
        }

        // если мы 33 раза добавим 1/33 то должны получить 1 а получим:
        // 0.9999999999999993
        assertThat(sum).isNotEqualTo(1);
    }

}
