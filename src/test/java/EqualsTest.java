import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.junit.Test;

import java.util.ArrayList;

import static org.assertj.core.api.Java6Assertions.assertThat;

public class EqualsTest {

    @Test
    public void should_filter_out_by_lastName() {
        ArrayList<Character> characters = new ArrayList<>();
        characters.add(new Character("Jaime", "Lannister", Sex.MALE));
        characters.add(new Character("Ned", "Stark", Sex.MALE));
        characters.add(new Character("Tyrion", "Lannister", Sex.MALE));
        characters.add(new Character("Cersei", "Lannister", Sex.FEMALE));
        characters.add(new Character("Sansa", "Stark", Sex.FEMALE));
        characters.add(new Character("Jon", "Snow", Sex.MALE));


        ArrayList<Character> lannisters = new ArrayList<>();

        // вывести на экран только Ланистеров:
        for (Character character : characters) {
            if ("Lannister".equals(character.getLastName())) {
                lannisters.add(character);
            }
        }

        assertThat(lannisters).hasSize(3);

    }

    @Test
    public void should_filter_out_by_object_compare() {
        ArrayList<Character> characters = new ArrayList<>();
        characters.add(new Character("Jaime", "Lannister", Sex.MALE));
        characters.add(new Character("Ned", "Stark", Sex.MALE));
        characters.add(new Character("Tyrion", "Lannister", Sex.MALE));
        characters.add(new Character("Cersei", "Lannister", Sex.FEMALE));
        characters.add(new Character("Sansa", "Stark", Sex.FEMALE));
        characters.add(new Character("Jon", "Snow", Sex.MALE));

        // есть ли в списке Джон Сноу?
        boolean jonFound = false; // это переменна логического типа. Используется для хранения true/false или да/нет, найден/не найден и что вы еще придумаете
        for (Character character : characters) {
            if (character.equals(new Character("Jon", "Snow", Sex.FEMALE))) {
                jonFound = true;
                break; // Выйти из цилка. Прекратить поиск по списку, потому что мы жуе наши Джона и нет смысла перебирать далше
            }
        }

        assertThat(jonFound).isTrue();
    }

}

@Data
@AllArgsConstructor
@EqualsAndHashCode(of = {"firstName", "lastName"})
class Character {
    private String firstName;
    private String lastName;
    private Sex sex;
}

enum Sex {
    MALE,
    FEMALE
}
