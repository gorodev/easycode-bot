import org.junit.Test;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class LoopsTest {

    @Test
    public void should_repeat_1_time() {
        int i = 100;
        do {
            i = i + 1; // блок выполниться 1 раз, потому что перед первым шагом проверки нет
        } while (i < 5); // после этой проверки программы выйдет из цикла

        assertThat(i).isEqualTo(101);
    }

    @Test
    public void should_skip_loop() {
        int i = 100;
        while (i < 5) { // блок не выполниться потому что 100 > 5
            System.out.println("Hello, World!");
            i = i + 1;
        }
        assertThat(i).isEqualTo(100); // значение i не поменялось
    }

    @Test
    public void should_repeat_5_times() {
        int sum = 0;

        for (int i = 0; i < 5; i++) { // условие цикла
            sum = sum + 10; // тело цикла
        }
        assertThat(sum).isEqualTo(50);
    }

}