import org.junit.Test;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class ArraysTest {

    @Test
    public void should_add_to_array() {
        String[] books = new String[3]; // выделили в памяти участок для 3х элементов

        // в программировани нумерация с нуля
        books[0] = "Thinking Java"; //запись в первую ячейку
        books[1] = "Core Java"; //во 2ю
        books[2] = "Cracking the coding interview";//в 3ю

        /*
          в памяти компьютера это сохраниться вот так:
          ["Thinking Java"]["Core Java"]["Cracking the coding interview"]
                    0              1                     2
        */

        assertThat(books[2]).isEqualTo("Cracking the coding interview");
    }

    @Test
    public void should_iterate_classic_for() {
        String[] books = new String[]{
                "Thinking Java",
                "Core Java",
                "Cracking the coding interview"
        };

        String allBooks = "";

        for (int i = 0; i < books.length; i++) {
            allBooks = allBooks + books[i] + ", ";
        }

        assertThat(allBooks).isEqualTo("Thinking Java, Core Java, Cracking the coding interview, ");
    }

    @Test
    public void should_iterate_foreach() {
        String[] books = new String[]{
                "Thinking Java",
                "Core Java",
                "Cracking the coding interview"
        };

        String allBooks = "";

        for (String book : books) {
            allBooks = allBooks + book + ", ";
        }

        assertThat(allBooks).isEqualTo("Thinking Java, Core Java, Cracking the coding interview, ");
    }
}
