package lesson_9;

import lesson_9.ingot.AluminumBar;
import lesson_9.ingot.GolderBar;
import lesson_9.ingot.PhysicalObject;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class InheritanceTest {

    @Test
    public void should_get_area() {
        // given
        var goldenBar = new GolderBar(5, 10);
        var aluminumBar = new AluminumBar(5, 10);

        // then
        assertThat(goldenBar.getMass()).isEqualTo(96.6);
        assertThat(aluminumBar.getMass()).isEqualTo(13.5);
        assertThat(goldenBar.getPressure()).isEqualTo(94.668);
        assertThat(aluminumBar.getPressure()).isEqualTo(13.23);
    }

    @Test
    public void should_get_total_mass() {
        // given
        var physicalObjects = new PhysicalObject[]{
                new GolderBar(1.2, 4),
                new GolderBar(2.3, 6),
                new AluminumBar(5.4, 0.2),
                new AluminumBar(2.6, 3),
        };

        // when
        var totalMass = 0;

        for (var phyObj : physicalObjects) {
            totalMass += phyObj.getMass();
        }

        // then
        assertThat(totalMass).isEqualTo(88);
    }

    @Test
    public void should_get_static() {
        assertThat(PhysicalObject.GRAVITY).isEqualTo(9.8);
    }


}


