package lesson_9.ingot;

public class GolderBar extends PhysicalObject {

    public GolderBar(double volume, double area) {
        super(volume, area);
    }

    // implementation of an abstract parent method.
    @Override
    double getDensity() {
        return 19.32;
    }
}
