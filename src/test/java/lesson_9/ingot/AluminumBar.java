package lesson_9.ingot;

public class AluminumBar extends PhysicalObject {

    public AluminumBar(double volume, double area) {
        super(volume, area);
    }

    // implementation of an abstract parent method.
    @Override
    double getDensity() {
        return 2.7;
    }
}
