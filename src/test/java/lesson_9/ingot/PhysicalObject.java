package lesson_9.ingot;


public abstract class PhysicalObject {

    public static double GRAVITY = 9.8;

    private final double volume;
    private final double area;

    PhysicalObject(double volume, double area) {
        this.volume = volume;
        this.area = area;
    }

    // abstract methods are implemented in descendants
    abstract double getDensity();

    public double getMass() {
        return getDensity() * volume;
    }

    public double getPressure() {
        return getWeight() / area;
    }

    /////////////////////////
    ///      private      ///
    /////////////////////////

    private double getWeight() {
        return getMass() * GRAVITY;
    }
}
