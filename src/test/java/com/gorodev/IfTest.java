package com.gorodev;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class IfTest {

    @Test
    public void should_check_condition() {
        // given
        int a = 27;
        int b = 0;

        // when
        if (a % 3 == 0) {
            b = 33;
        }

        // then
        assertThat(b).isEqualTo(77);
    }


    @Test
    public void should_check_if_else() {
        // given
        int a = 27;
        int b = 0;

        // when
        if (a % 5 == 0) {
            b = 5;
        } else if (a % 3 == 0) {
            b = 3;
        }

        // then
        assertThat(a).isEqualTo(27);
        assertThat(b).isEqualTo(3);
    }
}
