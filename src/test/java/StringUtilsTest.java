import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class StringUtilsTest {

    @Test
    public void should_check_nullable() {
        String nullString = null;

        assertThat(StringUtils.isEmpty(nullString)).isTrue();
    }

    @Test
    public void should_check_zero_length() {
        String nullString = "";

        assertThat(StringUtils.isEmpty(nullString)).isTrue();
    }


    @Test
    public void should_check_non_zero_length() {
        String nullString = " ";

        assertThat(StringUtils.isEmpty(nullString)).isFalse(); // пробелы считаются как не пустая строка
    }

}
