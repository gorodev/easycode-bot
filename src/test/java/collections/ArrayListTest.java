package collections;

import org.junit.Test;

import java.util.ArrayList;

import static org.assertj.core.api.Java6Assertions.assertThat;

public class ArrayListTest {

    @Test
    public void should_add_elements() {
        // другой синтаксис но суть таже - храним данные в памяти, можем доставать по индексу. Отлично! Мы больше не беспокоимся об управлении размером массива. За нас это делает "обертка" ArrayList которая внутри хранит массив, и в случаее его переполнения - создаем новый в копирует туда все старые значения.
        ArrayList<String> characters = new ArrayList<>();
        characters.add("Jaime Lannister");
        characters.add("Tyrion Lannister");

        assertThat(characters).containsExactly("Jaime Lannister", "Tyrion Lannister");
    }

    @Test
    public void should_add_duplicates() {
        ArrayList<String> characters = new ArrayList<>();
        characters.add("Jaime Lannister");
        characters.add("Tyrion Lannister");
        characters.add("Jon Snow");
        characters.add("Jon Snow");
        characters.add("Jon Snow");

        assertThat(characters).hasSize(6);
    }
}
