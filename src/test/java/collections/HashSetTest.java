package collections;

import org.junit.Test;

import java.util.HashSet;

import static org.assertj.core.api.Java6Assertions.assertThat;

public class HashSetTest {

    @Test
    public void should_add_without_duplicates() {
        HashSet<String> characters = new HashSet<>();
        characters.add("Jaime Lannister");
        characters.add("Tyrion Lannister");
        characters.add("Jon Snow");
        characters.add("Jon Snow");
        characters.add("Jon Snow");

        assertThat(characters.size()).isEqualTo(3);
        assertThat(characters).containsExactlyInAnyOrder("Jon Snow", "Tyrion Lannister", "Jaime Lannister");
    }

}
